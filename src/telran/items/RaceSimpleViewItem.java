package telran.items;

import telran.RaceThreadSimpleResult;
import telran.menu.InputOutput;
import telran.menu.Item;

public class RaceSimpleViewItem implements Item {

    private final InputOutput inputOutput;

    public RaceSimpleViewItem(InputOutput inputOutput) {
        super();
        this.inputOutput = inputOutput;
    }

    @Override
    public String displayName() {
        return "Start Threads-Race Game";
    }

    @Override
    public void perform() {
        int nThreads = inputOutput.inputInteger("Enter number of threads");
        int distance = inputOutput.inputInteger("Enter distance");

        RaceThreadSimpleResult[] threads = new RaceThreadSimpleResult[nThreads];

        startThreads(nThreads, distance, threads);
        joinRacers(threads);

        int gameWinner = RaceThreadSimpleResult.getWinner();
        inputOutput.displayLine(String.format("Congratulations to thread # %d", gameWinner));

        RaceThreadSimpleResult.clearWinner();
    }

    private void startThreads(int nThreads, int distance, RaceThreadSimpleResult[] threads) {
        for (int i = 0; i < nThreads; i++) {
            threads[i] = new RaceThreadSimpleResult("" + (i + 1), distance, inputOutput);
            threads[i].start();
        }
    }

    private void joinRacers(RaceThreadSimpleResult[] threads) {
        for (RaceThreadSimpleResult t : threads) {
            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
