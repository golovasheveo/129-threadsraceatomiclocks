package telran;

import telran.items.RaceSimpleViewItem;
import telran.items.RaceTableViewResultItem;
import telran.menu.*;

public class ThreadsRaceAppl {
    public static void main(String[] args) {
        InputOutput inputOutput = new ConsoleInputOutput();
        Item[] items = {
                new RaceSimpleViewItem(inputOutput),
                new RaceTableViewResultItem(inputOutput),
                new ExitItem(),
        };
        Menu menu = new Menu(items, inputOutput);
        menu.menuRun();
    }
}
