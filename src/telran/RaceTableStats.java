package telran;

import telran.menu.InputOutput;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class RaceTableStats extends Thread {
    private static final int MIN = 2;
    private static final int MAX = 5;
    public static ArrayList<Integer[]> order = new ArrayList<>();
    int distance;
    InputOutput inputOutput;

    private static final Lock lock = new ReentrantLock(true);


    public RaceTableStats(String name, int distance, InputOutput inputOutput) {
        super(name);
        this.inputOutput = inputOutput;
        this.distance = distance;
    }

    public static void clearStats() {
        order.clear();
    }

    @Override
    public void run() {
        Instant start = Instant.now();
        for(int i = 0; i<distance; i++) {
            waitAndDisplayName();
        }
        int duration = (int)ChronoUnit.MILLIS.between(start, Instant.now());

        try {
            lock.lock();
            Integer[] toAdd = new Integer[2];
            toAdd[0] = Integer.parseInt(getName());
            toAdd[1] = duration;
            order.add(toAdd);
        }finally {
            lock.unlock();
        }
    }

    private void waitAndDisplayName() {
        try {
            sleep(getRandInt());
        } catch (InterruptedException e) {}
        inputOutput.displayLine(getName());
    }

    private long getRandInt() {
        return (long) (Math.random() * ((long) RaceTableStats.MAX
                - (long) RaceTableStats.MIN + 1)
                + (long) RaceTableStats.MIN);
    }
}
