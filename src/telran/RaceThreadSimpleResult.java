package telran;

import telran.menu.InputOutput;

import java.util.concurrent.atomic.AtomicInteger;

public class RaceThreadSimpleResult extends Thread {
    private static final int MIN = 2;
    private static final int MAX = 5;
    private static final AtomicInteger winner = new AtomicInteger(0);

    int distance;
    InputOutput inputOutput;

    public RaceThreadSimpleResult(String name, int distance, InputOutput inputOutput) {
        super(name);
        this.inputOutput = inputOutput;
        this.distance = distance;

    }

    public static void clearWinner() {
        winner.set(0);
    }

    public static int getWinner() {
        return winner.get();
    }

    @Override
    public void run() {
        int threadId = Integer.parseInt(getName());
        for(int i = 0; i < distance; i++) {
            try {
                sleep(getRandInt());
            } catch (InterruptedException e) {}
            inputOutput.displayLine(threadId);
        }
        winner.compareAndSet(0, threadId);
    }

    private long getRandInt() {
        return (long) (Math.random() * ((long) RaceThreadSimpleResult.MAX
                - (long) RaceThreadSimpleResult.MIN + 1)
                + (long) RaceThreadSimpleResult.MIN);
    }
}
